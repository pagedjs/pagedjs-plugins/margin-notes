# Script for marginal notes 

This folder proposes a script for marginal notes in paged.js. 

The script creates marginal notes from `span` elements and move notes if notes overlapping or if the last note overflow at the end of the page.

![Screenshot of the example](screenshot.png)

## How to use the script

Just add the `marginNotes.js` file  of this folder to your project and link the javascript in the head of your document.

```HTML
<script src="path/to/file/marginNotes.js" type="text/javascript"></script>
```

By default, this script transform all `.margin-note` elements fo your document into marginal notes. You can change the class of your note in the js file at the first line.


## Notes in HTML

In your HTML, the note must be a span insert in the text. Like this:

```HTML
Donec tincidunt, odio vel vestibulum sollicitudin, nibh dolor tempor sapien, ac laoreet 
sem felis ut purus.&#8239;<span class="margin-note">Vestibulum neque ex, ullamcorper sit 
amet diam sed, pharetra laoreet sem.</span> Morbi cursus bibendum consectetur. Nullam vel 
lacus congue nibh pulvinar maximus sit amet eu risus. Curabitur semper odio mauris, nec 
imperdiet velit pharetra non. Aenean accumsan nulla ac ex iaculis interdum.
```


## Positionning note

It is possible to place the notes in four ways.

- 'outside': the left pages notes positonned on the margin left and the right pages notes on the margin right. This is the default value.
- 'inside': the left pages notes positonned on the margin right and the right pages notes on the margin left.
- 'left': the notes of both pages are positinoned on the margin left.
- 'right': the notes of both pages are positinoned on the margin left.

You can set the way you want in the second line of the `marginNotes.js` file:

```JS
let notesFloat = "outside";
```

## Styling notes

By default, the width of the notes is set on the corresponding margin where the notes are positioning. 
You can change this width by playing with `padding-left` and `padding-right` of your note elements.

To apply specific style depending on the left or right pages, uses the following CSS (where `margin-note` is the class of your notes):

```CSS
.pagedjs_left_page .margin-note {
    padding-left: 40px;
    padding-right: 20px;
}
.pagedjs_right_page .margin-note {
    padding-left: 20px;
    padding-right: 40px;
}
```

However, if you want to change the CSS `width` value, add `!important` at the end (sorry, it's because the script is not directly integrate to paged.js).

It's possible to change the styles of call notes and marker notes directly in your stylesheet like in the following code, ass this CSS (where `margin-note` is the class of your notes):

```CSS
.note-call_margin-note{
    color: blue; 
}
.note-marker_margin-note{
    color: violet; 
}
```


## Possible improvement

ATTENTION: For now, there is no current way with paged.js to break the note element into two pages. If all the margin notes of the page don't fit on the page, you will have an alert.

- Authorize multiple type of margin notes.
- Choose if it's the first line or the last line of the margin notes that is aligned with the call notes.
